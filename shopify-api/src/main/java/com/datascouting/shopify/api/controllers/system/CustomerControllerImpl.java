package com.datascouting.shopify.api.controllers.system;

import com.datascouting.shopify.api.models.Customer;
import com.datascouting.shopify.api.services.system.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Chrisostomos Bakouras
 */
@RestController
@RequestMapping("/customers")
public class CustomerControllerImpl implements CustomerController {
    @Autowired
    private CustomerService customerService;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Customer>> get() {
        List<Customer> customers = customerService.get();

        return ResponseEntity.ok(customers);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Customer> getOne(@PathVariable("id") Long id) {
        Customer customer = customerService.getOne(id);

        return ResponseEntity.ok(customer);
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Customer> post(@RequestBody Customer customer) {
        Customer createdCustomer = customerService.create(customer);

        URI uri = linkTo(methodOn(CustomerControllerImpl.class).getOne(createdCustomer.getId())).toUri();

        return ResponseEntity
                .created(uri)
                .body(createdCustomer);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Customer> put(@PathVariable Long id, @RequestBody Customer customer) {
        Customer updatedCustomer = customerService.update(id, customer);

        return ResponseEntity.ok(updatedCustomer);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        customerService.delete(id);

        return ResponseEntity
                .noContent()
                .build();
    }
}
