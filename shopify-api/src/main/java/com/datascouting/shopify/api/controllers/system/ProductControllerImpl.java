package com.datascouting.shopify.api.controllers.system;

import com.datascouting.shopify.api.models.Product;
import com.datascouting.shopify.api.services.system.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Chrisostomos Bakouras
 */
@RestController
@RequestMapping("/products")
public class ProductControllerImpl implements ProductController {
    @Autowired
    private ProductService productService;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Product>> get() {
        List<Product> products = productService.get();

        return ResponseEntity.ok(products);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> getOne(Long id) {
        Product product = productService.getOne(id);

        return ResponseEntity.ok(product);
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Product> post(Product product) {
        Product createdProduct = productService.create(product);

        URI uri = linkTo(methodOn(CustomerControllerImpl.class).getOne(createdProduct.getId())).toUri();

        return ResponseEntity
                .created(uri)
                .body(createdProduct);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Product> put(Long id, Product product) {
        Product updatedProduct = productService.update(id, product);

        return ResponseEntity.ok(updatedProduct);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(Long id) {
        productService.delete(id);

        return ResponseEntity
                .noContent()
                .build();
    }
}
