package com.datascouting.shopify.api.services.system;

import com.datascouting.shopify.api.models.Company;
import com.datascouting.shopify.api.repositories.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author Chrisostomos Bakouras
 */
@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public List<Company> get() {
        return companyRepository.findAll();
    }

    @Override
    public Company getOne(Long id) {
        return companyRepository.findOne(id);
    }

    @Override
    public Company create(Company company) {
        return companyRepository.saveAndFlush(company);
    }

    @Override
    public Company update(Long id, Company company) {
        if (!Objects.equals(id, company.getId())) {
            companyRepository.delete(id);
        }

        return companyRepository.save(company);
    }

    @Override
    public void delete(Long id) {
        companyRepository.delete(id);
    }
}
