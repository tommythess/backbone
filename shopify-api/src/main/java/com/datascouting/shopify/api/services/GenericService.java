package com.datascouting.shopify.api.services;

import java.util.List;

/**
 * @author Chrisostomos Bakouras
 */
public interface GenericService<T> {
    List<T> get();

    T getOne(Long id);

    T create(T entity);

    T update(Long id, T entity);

    void delete(Long id);
}
