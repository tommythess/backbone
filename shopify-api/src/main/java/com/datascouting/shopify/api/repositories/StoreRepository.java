package com.datascouting.shopify.api.repositories;

import com.datascouting.shopify.api.models.Company;
import com.datascouting.shopify.api.models.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Chrisostomos Bakouras
 */
@Repository
public interface StoreRepository extends JpaRepository<Store, Long> {
    List<Store> findByCompany(Company company);

    Store findByCompanyAndId(Company company, Long id);

    Void deleteByCompanyAndId(Company company, Long id);
}
