package com.datascouting.shopify.api.repositories;

import com.datascouting.shopify.api.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Chrisostomos Bakouras
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
