package com.datascouting.shopify.api.controllers.system;

import com.datascouting.shopify.api.controllers.GenericController;
import com.datascouting.shopify.api.models.Company;

/**
 * @author Chrisostomos Bakouras
 */
public interface CompanyController extends GenericController<Company> {
}
