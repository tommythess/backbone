package com.datascouting.shopify.api.controllers.system;

import com.datascouting.shopify.api.controllers.GenericController;
import com.datascouting.shopify.api.models.Customer;

/**
 * @author Chrisostomos Bakouras
 */
public interface CustomerController extends GenericController<Customer> {
}
