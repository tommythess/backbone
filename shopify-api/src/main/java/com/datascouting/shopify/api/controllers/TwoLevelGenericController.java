package com.datascouting.shopify.api.controllers;

import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * @author Chrisostomos Bakouras
 */
public interface TwoLevelGenericController<T> {
    ResponseEntity<List<T>> get(Long firstLevelId);

    ResponseEntity<T> getOne(Long firstLevelId, Long secondLevelId);

    ResponseEntity<T> post(Long firstLevelId, T entity);

    ResponseEntity<T> put(Long firstLevelId, Long secondLevelId, T entity);

    ResponseEntity<Void> delete(Long firstLevelId, Long secondLevelId);
}
