package com.datascouting.shopify.api.services;

import java.util.List;

/**
 * @author Chrisostomos Bakouras
 */
public interface TwoLevelGenericService<T> {
    List<T> get(Long firstLevelId);

    T getOne(Long firstLevelId, Long secondLevelId);

    T create(Long firstLevelId, T entity);

    T update(Long firstLevelId, Long secondLevelId, T entity);

    void delete(Long firstLevelId, Long secondLevelId);
}
