package com.datascouting.shopify.api.services.company;

import com.datascouting.shopify.api.models.Company;
import com.datascouting.shopify.api.models.Store;
import com.datascouting.shopify.api.repositories.CompanyRepository;
import com.datascouting.shopify.api.repositories.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author Chrisostomos Bakouras
 */
@Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public List<Store> get(Long companyId) {
        Company company = companyRepository.findOne(companyId);

        return storeRepository.findByCompany(company);
    }

    @Override
    public Store getOne(Long companyId, Long storeId) {
        Company company = companyRepository.findOne(companyId);

        return storeRepository.findByCompanyAndId(company, storeId);
    }

    @Override
    public Store create(Long companyId, Store Store) {
        return storeRepository.saveAndFlush(Store);
    }

    @Override
    public Store update(Long companyId, Long storeId, Store Store) {
        if (!Objects.equals(storeId, Store.getId())) {
            storeRepository.delete(storeId);
        }

        return storeRepository.save(Store);
    }

    @Override
    public void delete(Long companyId, Long storeId) {
        Company company = companyRepository.findOne(companyId);

        storeRepository.deleteByCompanyAndId(company, storeId);
    }
}
