package com.datascouting.shopify.api.services.system;

import com.datascouting.shopify.api.models.Company;
import com.datascouting.shopify.api.services.GenericService;

/**
 * @author Chrisostomos Bakouras
 */
public interface CompanyService extends GenericService<Company> {
}
