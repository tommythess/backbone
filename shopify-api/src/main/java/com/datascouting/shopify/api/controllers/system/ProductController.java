package com.datascouting.shopify.api.controllers.system;

import com.datascouting.shopify.api.controllers.GenericController;
import com.datascouting.shopify.api.models.Product;

/**
 * @author Chrisostomos Bakouras
 */
public interface ProductController extends GenericController<Product> {
}
