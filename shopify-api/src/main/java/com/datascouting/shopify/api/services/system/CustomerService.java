package com.datascouting.shopify.api.services.system;

import com.datascouting.shopify.api.models.Customer;
import com.datascouting.shopify.api.services.GenericService;

/**
 * @author Chrisostomos Bakouras
 */
public interface CustomerService extends GenericService<Customer> {
}
