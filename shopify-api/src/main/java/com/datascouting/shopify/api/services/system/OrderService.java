package com.datascouting.shopify.api.services.system;

import com.datascouting.shopify.api.models.Order;
import com.datascouting.shopify.api.services.GenericService;

/**
 * @author Chrisostomos Bakouras
 */
public interface OrderService extends GenericService<Order> {
}
