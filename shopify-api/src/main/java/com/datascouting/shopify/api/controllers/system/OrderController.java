package com.datascouting.shopify.api.controllers.system;

import com.datascouting.shopify.api.controllers.GenericController;
import com.datascouting.shopify.api.models.Order;

/**
 * @author Chrisostomos Bakouras
 */
public interface OrderController extends GenericController<Order> {
}
