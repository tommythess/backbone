package com.datascouting.shopify.api.controllers.company;

import com.datascouting.shopify.api.controllers.system.CustomerControllerImpl;
import com.datascouting.shopify.api.models.Store;
import com.datascouting.shopify.api.services.company.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Chrisostomos Bakouras
 */
@RestController
@RequestMapping("/companies/{companyId}/stores")
public class StoreControllerImpl implements StoreController {
    @Autowired
    private StoreService storeService;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Store>> get(@PathVariable("companyId") Long companyId) {
        List<Store> stores = storeService.get(companyId);

        return ResponseEntity.ok(stores);
    }

    @Override
    @RequestMapping(value = "/{storeId}", method = RequestMethod.GET)
    public ResponseEntity<Store> getOne(@PathVariable("companyId") Long companyId, @PathVariable("storeId") Long storeId) {
        Store store = storeService.getOne(companyId, storeId);

        return ResponseEntity.ok(store);
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Store> post(@PathVariable("companyId") Long companyId, @RequestBody Store store) {
        Store createdCustomer = storeService.create(companyId, store);

        URI uri = ControllerLinkBuilder.linkTo(methodOn(CustomerControllerImpl.class).getOne(createdCustomer.getId())).toUri();

        return ResponseEntity
                .created(uri)
                .body(createdCustomer);
    }

    @Override
    @RequestMapping(value = "/{storeId}", method = RequestMethod.PUT)
    public ResponseEntity<Store> put(@PathVariable("companyId") Long companyId, @PathVariable Long storeId, @RequestBody Store store) {
        Store updatedStore = storeService.update(companyId, storeId, store);

        return ResponseEntity.ok(updatedStore);
    }

    @Override
    @RequestMapping(value = "/{storeId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("companyId") Long companyId, @PathVariable Long storeId) {
        storeService.delete(companyId, storeId);

        return ResponseEntity
                .noContent()
                .build();
    }
}
