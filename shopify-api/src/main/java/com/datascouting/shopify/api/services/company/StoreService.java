package com.datascouting.shopify.api.services.company;

import com.datascouting.shopify.api.models.Store;
import com.datascouting.shopify.api.services.TwoLevelGenericService;

/**
 * @author Chrisostomos Bakouras
 */
public interface StoreService extends TwoLevelGenericService<Store> {
}
