package com.datascouting.shopify.api.services.system;

import com.datascouting.shopify.api.models.Product;
import com.datascouting.shopify.api.services.GenericService;

/**
 * @author Chrisostomos Bakouras
 */
public interface ProductService extends GenericService<Product> {
}
