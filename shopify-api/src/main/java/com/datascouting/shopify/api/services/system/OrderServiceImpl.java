package com.datascouting.shopify.api.services.system;

import com.datascouting.shopify.api.models.Order;
import com.datascouting.shopify.api.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author Chrisostomos Bakouras
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<Order> get() {
        return orderRepository.findAll();
    }

    @Override
    public Order getOne(Long id) {
        return orderRepository.findOne(id);
    }

    @Override
    public Order create(Order Order) {
        return orderRepository.saveAndFlush(Order);
    }

    @Override
    public Order update(Long id, Order Order) {
        if (!Objects.equals(id, Order.getId())) {
            orderRepository.delete(id);
        }

        return orderRepository.save(Order);
    }

    @Override
    public void delete(Long id) {
        orderRepository.delete(id);
    }
}