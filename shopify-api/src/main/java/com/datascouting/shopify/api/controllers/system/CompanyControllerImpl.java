package com.datascouting.shopify.api.controllers.system;

import com.datascouting.shopify.api.models.Company;
import com.datascouting.shopify.api.services.system.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Chrisostomos Bakouras
 */
@RestController
@RequestMapping("/companies")
public class CompanyControllerImpl implements CompanyController {
    @Autowired
    private CompanyService companyService;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Company>> get() {
        List<Company> Companies = companyService.get();

        return ResponseEntity.ok(Companies);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Company> getOne(@PathVariable("id") Long id) {
        Company Company = companyService.getOne(id);

        return ResponseEntity.ok(Company);
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Company> post(@RequestBody Company Company) {
        Company createdCompany = companyService.create(Company);

        URI uri = linkTo(methodOn(CompanyControllerImpl.class).getOne(createdCompany.getId())).toUri();

        return ResponseEntity
                .created(uri)
                .body(createdCompany);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Company> put(@PathVariable Long id, @RequestBody Company Company) {
        Company updatedCompany = companyService.update(id, Company);

        return ResponseEntity.ok(updatedCompany);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        companyService.delete(id);

        return ResponseEntity
                .noContent()
                .build();
    }
}
