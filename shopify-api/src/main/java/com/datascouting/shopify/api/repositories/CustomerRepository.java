package com.datascouting.shopify.api.repositories;

import com.datascouting.shopify.api.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Chrisostomos Bakouras
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
