package com.datascouting.shopify.api.controllers.company;

import com.datascouting.shopify.api.controllers.TwoLevelGenericController;
import com.datascouting.shopify.api.models.Store;

/**
 * @author Chrisostomos Bakouras
 */
public interface StoreController extends TwoLevelGenericController<Store> {
}
