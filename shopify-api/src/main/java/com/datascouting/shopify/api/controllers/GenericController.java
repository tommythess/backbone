package com.datascouting.shopify.api.controllers;

import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * @author Chrisostomos Bakouras
 */
public interface GenericController<T> {
    ResponseEntity<List<T>> get();

    ResponseEntity<T> getOne(Long id);

    ResponseEntity<T> post(T entity);

    ResponseEntity<T> put(Long id, T entity);

    ResponseEntity<Void> delete(Long id);
}
