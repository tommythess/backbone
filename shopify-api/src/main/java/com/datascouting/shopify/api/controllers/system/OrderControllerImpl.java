package com.datascouting.shopify.api.controllers.system;

import com.datascouting.shopify.api.models.Order;
import com.datascouting.shopify.api.services.system.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Chrisostomos Bakouras
 */
@Controller
public class OrderControllerImpl implements OrderController {
    @Autowired
    private OrderService orderService;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Order>> get() {
        List<Order> customers = orderService.get();

        return ResponseEntity.ok(customers);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Order> getOne(@PathVariable("id") Long id) {
        Order customer = orderService.getOne(id);

        return ResponseEntity.ok(customer);
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Order> post(@RequestBody Order customer) {
        Order createdOrder = orderService.create(customer);

        URI uri = linkTo(methodOn(OrderControllerImpl.class).getOne(createdOrder.getId())).toUri();

        return ResponseEntity
                .created(uri)
                .body(createdOrder);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Order> put(@PathVariable Long id, @RequestBody Order customer) {
        Order updatedOrder = orderService.update(id, customer);

        return ResponseEntity.ok(updatedOrder);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        orderService.delete(id);

        return ResponseEntity
                .noContent()
                .build();
    }
}
