INSERT INTO customer (id, name, surname, email, phone) VALUES (1, 'Stavros', 'Karikatouras', 'Kari', '6931234567');
INSERT INTO customer (id, name, surname, email, phone) VALUES (2, 'Mixalis', 'Trobadouros', 'Trobas', '6932345678');
INSERT INTO customer (id, name, surname, email, phone) VALUES (3, 'Stamatis', 'Grigoriou', 'Gregor', '6933456789');
INSERT INTO customer (id, name, surname, email, phone) VALUES (4, 'Theomenis', 'Diabolakis', 'evil-saint', '6941234567');

INSERT INTO company (id, company, address, postal_code, city) VALUES (1, 'Mparoufopouls A.E.', 'Thessalonikis 29', '62000', 'Serres');
INSERT INTO company (id, company, address, postal_code, city) VALUES (2, 'Karperos and Bros.', 'Makrugianni 123', '53412', 'Thessaloniki');
INSERT INTO company (id, company, address, postal_code, city) VALUES (3, 'Shoppy', 'Turokomeiou 33', '12333', 'Larisa');

INSERT INTO store (id, address, postal_code, city, company_id) VALUES (1, 'Thessalonikis 29', '62000', 'Serres', 1);
INSERT INTO store (id, address, postal_code, city, company_id) VALUES (2, 'Makedonomaxwn 123', '62000', 'Serres', 1);
INSERT INTO store (id, address, postal_code, city, company_id) VALUES (3, 'Glinou 2', '62000', 'Thessaloniki', 1);
INSERT INTO store (id, address, postal_code, city, company_id) VALUES (4, 'Makrugianni 123', '53412', 'Thessaloniki', 2);
INSERT INTO store (id, address, postal_code, city, company_id) VALUES (5, 'Turokomeiou 33', '12333', 'Larisa', 3);

